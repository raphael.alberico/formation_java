

    Créer une branche documentation;
    Créer un fichier README.md dans le dossier qui contient l'exercice de POO;
    Faire une description brève de l'exercice dans le README.md;
    Commiter;
    Pousser la branche documentation, git push -u origin documentation;
    Créer un Merge Request sur le dépôt distant


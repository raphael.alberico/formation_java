package com.example;

import java.util.Arrays;

//Java program Illustrating Set Interface

//Importing utility classes
import java.util.HashSet;
import java.util.Set;

//Main class
public class Application {

    // Main driver method
    public static void main(String[] args) {

        // // Demonstrating Set using HashSet
//        // Declaring object of type String
//        Set<Integer> hash_Set = new HashSet<Integer>();
//
//        // Adding elements to the Set
//        // using add() method
//        hash_Set.add(2);
//        hash_Set.add(34);
//        hash_Set.add(2);
//        hash_Set.add(42);
//        hash_Set.add(60);

        Set<Integer> hash_Set = new HashSet<Integer>(
                Arrays.asList(2, 34, 2, 42, 60));

        // Printing elements of HashSet object
        System.out.println(hash_Set);
    }
}
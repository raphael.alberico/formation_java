//package com.example.demo;
//
//import java.util.List;
//
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository // (collectionResourceRel = "commune", path = "commune")
//public interface PersonneRepository extends CrudRepository<Personne, Long> {
//    List<Personne> findByNom(String nom);
//
//}

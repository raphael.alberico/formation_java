package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

//    @Autowired
//    private PersonneRepository personneRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    // Run at startup of the application
//    @Bean
//    public CommandLineRunner demo() {
//        return (args) -> {
//            System.err.println("TOTO");
//            Personne plaineDesPalmistes = new Personne(
//                    "La Plaine des Palmistes");
//            personneRepository.save(plaineDesPalmistes);
//        };
//   }

}
